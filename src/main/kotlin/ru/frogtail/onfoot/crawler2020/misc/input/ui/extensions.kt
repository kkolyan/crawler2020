package ru.frogtail.onfoot.crawler2020.misc.input.ui

import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.Spatial
import com.jme3.scene.control.AbstractControl

fun <T: Spatial> T.addHoverControl(onHover: () -> Unit): T {
    addControl(HoverControlImpl(onHover))
    return this
}

private class HoverControlImpl(
    private val onHover: () -> Unit
) : AbstractControl(), HoverControl {
    override fun controlRender(rm: RenderManager?, vp: ViewPort?) = Unit

    override fun controlUpdate(tpf: Float) = Unit

    override fun oHover() {
        onHover.invoke()
    }
}