package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.data.Range
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.person.Person
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityIntent
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.CounterParty
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.StandardImpact
import ru.frogtail.onfoot.crawler2020.game.state.person.calculateCostLack
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector
import kotlin.math.absoluteValue

class AbilitySystem(
    private val game: Game,
    private val impactSystem: ImpactSystem
) {
    fun aim(
        actor: Person,
        ability: Ability,
        targetTile: GridVector?,
        targetPersonId: PersonId?
    ): AbilityIntent {
        if (!actor.template.abilities.contains(ability)) {
            return AbilityIntent.Invalid(ability, "ability not known by person")
        }
        val costLack = actor.calculateCostLack(ability)
        if (costLack.isNotEmpty()) {
            return AbilityIntent.Invalid(ability, "not enough: $costLack")
        }

        val actorLocation = game.board.persons.getA(actor.id)
            ?: error("WTF")

        val counterParty = when (targetPersonId == actor.id) {
            true -> CounterParty.Friend
            false -> CounterParty.Enemy
        }
        val compatibleCounterParties = setOf(CounterParty.Any) + counterParty

        return when (ability) {
            is Ability.Place -> {
                when {
                    targetTile != null -> {
                        val rejectionMessage = when (ability) {
                            is Ability.Place.Jump -> when {
                                !isFree(targetTile) -> "destination is occuplied"
                                targetTile.distanceTo(actorLocation) > ability.range.value -> "destination is too far"
                                else -> null
                            }
                            is Ability.Place.Charge -> {
                                val delta = targetTile.subtract(actorLocation)
                                when {
                                    !isFree(targetTile) -> "destination occupied"
                                    delta.east != 0 && delta.north != 0 -> "only straight chanrges allowed"
                                    delta.north == 0 && delta.east.absoluteValue > ability.range -> "destination is too far"
                                    delta.east == 0 && delta.north.absoluteValue > ability.range -> "destination is too far"
                                    else -> null
                                }
                            }
                            is Ability.Place.AOE -> when {
                                targetTile.distanceTo(actorLocation) > ability.range.value -> "destination is too far"
                                else -> null
                            }
                        }
                        when (rejectionMessage) {
                            null -> AbilityIntent.Valid.Place(targetTile, ability)
                            else -> AbilityIntent.Invalid(ability, rejectionMessage)
                        }
                    }
                    else -> AbilityIntent.Invalid(ability, "select tile")
                }
            }
            is Ability.NoTarget -> {
                AbilityIntent.Valid.NoTarget(ability)
            }
            is Ability.Person -> {
                when {
                    targetPersonId != null -> {
                        val targetLocation = game.board.persons.getA(targetPersonId)
                            ?: error("WTF")
                        val rejectionMessage = when (ability) {
                            is Ability.Person.Direct -> {
                                when {
                                    !compatibleCounterParties.contains(ability.counterParty) -> "cannot be applied to $counterParty"
                                    targetLocation.distanceTo(actorLocation) > ability.range.value -> "target is too far"
                                    else -> null
                                }
                            }
                        }
                        when (rejectionMessage) {
                            null -> AbilityIntent.Valid.Person(targetPersonId, ability)
                            else -> AbilityIntent.Invalid(ability, rejectionMessage)
                        }
                    }
                    else -> AbilityIntent.Invalid(ability, "select person (${ability.counterParty})")
                }
            }
        }
    }

    fun trigger(actor: Person, intent: AbilityIntent.Valid) {
        for ((resource, amount) in intent.ability.common.cost) {
            actor.withdrawResource(resource, amount)
        }
        val actorLocation = game.board.persons.getA(actor.id) ?: error("WTF")
        val stub = when (intent) {
            is AbilityIntent.Valid.Place -> when (intent.ability) {
                is Ability.Place.AOE -> {
                    applyAoe(actor, intent.ability.impact, intent.target, intent.ability.radius)
                }
                is Ability.Place.Jump, is Ability.Place.Charge -> {
                    game.board.persons.reLink(intent.target, actor.id)
                }
            }
            is AbilityIntent.Valid.Person -> when (intent.ability) {
                is Ability.Person.Direct -> {
                    val target = game.persons.getValue(intent.target)
                    impactSystem.acceptImpact(actor, intent.ability.impact, target)
                }
            }
            is AbilityIntent.Valid.NoTarget -> when (intent.ability) {
                is Ability.NoTarget.AOE -> {
                    applyAoe(actor, intent.ability.impact, actorLocation, intent.ability.radius)
                }
                is Ability.NoTarget.Self -> {
                    impactSystem.acceptImpact(actor, intent.ability.impact, actor)
                }
                is Ability.NoTarget.Stance -> Unit
            }
        }
        for ((resource, value) in intent.ability.refund) {
            actor.addResource(resource, value)
        }
        repeat(intent.ability.restoreRandomSlot) {
            val options = actor.template.initialStacks.flatMap { stack ->
                val cap = stack.value.size
                val size = actor.stacks.getValue(stack.key).size
                (size until cap).map { stack.key }
            }
            if (options.isNotEmpty()) {
                val slot = options.random(game.random)
                actor.addToSlot(slot, 1)
            }
        }
    }

    private fun isFree(place: GridVector): Boolean {
        return !game.board.walls.containsKey(place) && game.board.persons.getB(place) == null
    }

    private fun applyAoe(actor: Person, impact: StandardImpact, target: GridVector, radius: Range) {
        val affected = game.board.persons.b
            .filter { personId ->
                val place = game.board.persons.getA(personId)
                    ?: error("WTF")
                val epicenter = place.subtract(target)
                epicenter.length() <= radius.value
            }
            .map { game.persons.getValue(it) }
        for (person in affected) {
            impactSystem.acceptImpact(actor, impact, person)
        }
    }
}