package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.integration.Lifecycle

class LifecycleSystem(
    private val controlSystem: ControlSystem,
    private val aiSystem: AISystem,
    private val displaySystem: DisplaySystem,
    private val turnSystem: TurnSystem,
    private val game: Game
) : Lifecycle {

    override fun beforeInput() {
        controlSystem.beforeInput()
    }

    override fun beforeUpdate() {
        displaySystem.beforeUpdate()
        controlSystem.beforeUpdate()
        aiSystem.beforeUpdate()
        turnSystem.beforeUpdate()
    }

    override fun afterUpdate() {
    }

    override fun isQuit(): Boolean = game.control.quit
}