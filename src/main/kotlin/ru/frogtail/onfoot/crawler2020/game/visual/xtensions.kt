package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.control.AbstractControl
import ru.frogtail.onfoot.crawler2020.game.data.LayoutConstants
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector
import ru.frogtail.onfoot.crawler2020.misc.scene.Icons

fun Spatial.addUpdateControl(onUpdate: (tpf: Float) -> Unit) {
    addControl(object : AbstractControl() {
        override fun controlRender(rm: RenderManager?, vp: ViewPort?) = Unit
        override fun controlUpdate(tpf: Float) = onUpdate(tpf)
    })
}

fun <T: Spatial> T.setLocalTranslation(translation: GridVector): T {
    setLocalTranslation(translation.east.toFloat(), translation.north.toFloat())
    return this
}

fun <T : Spatial> T.setLocalTranslation(x: Float, y: Float): T {
    setLocalTranslation(x, y, 0f)
    return this
}

fun <T : Spatial> T.attachTo(node: Node): T {
    node.attachChild(this)
    return this
}

fun String.parseColor(): ColorRGBA = Colors.parseColor(this)

fun AssetManager.loadDefaultFont(): BitmapFont {
    return loadFont(LayoutConstants.FontAssetName)
}

fun AssetManager.loadIcon(icon: Icon, color: ColorRGBA? = null): Spatial {
    return Icons.createPicture(this,  icon, color)
}
