package ru.frogtail.onfoot.crawler2020.game.state.person.ability

import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector

sealed class AbilityIntent(
) {
    abstract val ability: Ability

    data class Invalid(
        override val ability: Ability,
        val message: String
    ) : AbilityIntent()

    sealed class Valid: AbilityIntent() {

        data class NoTarget(
            override val ability: Ability.NoTarget
        ) : Valid()

        data class Place(
            val target: GridVector,
            override val ability: Ability.Place
        ) : Valid()

        data class Person(
            val target: PersonId,
            override val ability: Ability.Person
        ) : Valid()
    }
}