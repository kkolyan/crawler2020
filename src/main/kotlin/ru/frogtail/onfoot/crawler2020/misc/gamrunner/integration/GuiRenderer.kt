package ru.frogtail.onfoot.crawler2020.misc.gamrunner.integration

import com.jme3.scene.Spatial

interface GuiRenderer {
    fun render(): Spatial
}