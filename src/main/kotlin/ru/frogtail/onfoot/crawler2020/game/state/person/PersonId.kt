package ru.frogtail.onfoot.crawler2020.game.state.person

data class PersonId(
    val value: Long
)