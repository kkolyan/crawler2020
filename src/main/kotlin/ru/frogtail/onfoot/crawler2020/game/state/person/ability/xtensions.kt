package ru.frogtail.onfoot.crawler2020.game.state.person.ability


fun AbilityIntent.isApplicable(): Boolean {
    return when (this) {
        is AbilityIntent.Invalid -> false
        is AbilityIntent.Valid -> true
    }
}

fun ContextualIntent.resolveAiming(): AbilityIntent {
    return when (this) {
        is ContextualIntent.Auto -> intent
        is ContextualIntent.Manual -> intent
    }
}