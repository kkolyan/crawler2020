package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityIntent
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.ContextualIntent
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector
import ru.frogtail.onfoot.crawler2020.misc.scene.Icons

class SceneCursor(
    private val assetManager: AssetManager,
    private val game: Game
) {
    private class Cursor(
        val icon: Spatial,
        val resolveLocation: () -> GridVector?
    ) {
        var prevLocation: GridVector? = null
    }

    fun render(): Spatial {
        val node = Node("Cursor Wrapper")
        val cursors = mutableListOf<Cursor>()
        cursors.add(Cursor(Icons.createPicture(assetManager, Icon("Dungeon Crawl Stone Soup Full/misc/cursor_green.png"))) {
            game.turnQueue.firstOrNull()
                ?.let {
                    game.board.persons.getA(it)
                }
        })
        cursors.add(Cursor(Icons.createPicture(assetManager, Icon("Dungeon Crawl Stone Soup Full/misc/cursor.png"))) {
            val aiming = resolveAiming(game)
            when(aiming) {
                null -> null
                is AbilityIntent.Valid.NoTarget -> null
                is AbilityIntent.Valid.Place -> aiming.target
                is AbilityIntent.Valid.Person -> null
                is AbilityIntent.Invalid -> null
            }
        })
        cursors.add(Cursor(Icons.createPicture(assetManager, Icon("Dungeon Crawl Stone Soup Full/misc/cursor_red.png"))) {
            val aiming = resolveAiming(game)
            when(aiming) {
                null -> null
                is AbilityIntent.Valid.NoTarget -> null
                is AbilityIntent.Valid.Place -> null
                is AbilityIntent.Valid.Person -> game.board.persons.getA(aiming.target)
                is AbilityIntent.Invalid -> null
            }
        })
        node.addUpdateControl {
            for (cursor in cursors) {
                val location = cursor.resolveLocation()
                if (location != cursor.prevLocation) {
                    if (location == null) {
                        node.detachChild(cursor.icon)
                    } else {
                        if (cursor.prevLocation == null) {
                            node.attachChild(cursor.icon)
                        }
                        cursor.icon.setLocalTranslation(location)
                    }
                    cursor.prevLocation = location
                }
            }
        }
        return node
    }

    private fun resolveAiming(game: Game): AbilityIntent? {
        val preparedAction = game.intent
        val aiming = when (preparedAction) {
            is ContextualIntent.Auto -> preparedAction.intent
            is ContextualIntent.Manual -> preparedAction.intent
            null -> null
        }
        return aiming
    }
}