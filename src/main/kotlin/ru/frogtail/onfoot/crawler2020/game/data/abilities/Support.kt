package ru.frogtail.onfoot.crawler2020.game.data.abilities

import ru.frogtail.onfoot.crawler2020.game.data.Range
import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityCommon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.CounterParty
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.StandardImpact

object Support {

    val Heal = Ability.Person.Direct(
        counterParty = CounterParty.Friend,
        range = Range.VeryFar,
        impact = StandardImpact(heal = 10),
        common = AbilityCommon(
            name = "Heal",
            icon = Icon("Dungeon Crawl Stone Soup Full/gui/spells/necromancy/borgnjors_revivification_old.png"),
            cost = mapOf(Resource.MagicRune01 to 1),
            refund = mapOf(Resource.MagicRune01 to 1)
        )
    )

    val DivineIntervention = Ability.NoTarget.Self(
        impact = StandardImpact(heal = 9001),
        common = AbilityCommon(
            name = "DivineIntervention",
            icon = Icon("Dungeon Crawl Stone Soup Full/gui/spells/necromancy/borgnjors_revivification_old.png")
        )
    )
}