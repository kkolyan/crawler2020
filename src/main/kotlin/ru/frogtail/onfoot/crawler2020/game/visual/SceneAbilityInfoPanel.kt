package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.color
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.resolveAiming
import ru.frogtail.onfoot.crawler2020.game.state.person.getResource
import ru.frogtail.onfoot.crawler2020.game.state.resolveTurnOwner

class SceneAbilityInfoPanel(
    private val game: Game,
    private val assetManager: AssetManager
) {

    fun render(): Spatial {
        return createReactiveNode(
            name = "Ability Info",
            resolveState = { game.intent?.resolveAiming()?.ability ?: game.control.pointedAbility  },
            renderContent = { node, ability ->
                val actor = game.resolveTurnOwner()
                if (ability != null && actor != null) {
                    var offset = 0f
                    for ((resource, amount) in ability.cost) {
                        repeat(amount) { i ->
                            val color = when {
                                i >= actor.getResource(resource) -> Colors.parseColor("F00")
                                else -> null
                            }
                            assetManager.loadIcon(resource.icon, color)
                                .setLocalTranslation(offset, 0f)
                                .attachTo(node)
                            offset++
                        }
                    }
                    BitmapText(assetManager.loadDefaultFont())
                        .also {
                            it.size = 0.25f
                            it.text = ability.common.name.color("FF0")
                            val boxWidth = 5f
                            it.setBox(Rectangle(offset, 1f, boxWidth, 1f))
                            offset += boxWidth
                            it.alignment = BitmapFont.Align.Center
                            it.verticalAlignment = BitmapFont.VAlign.Center
                        }
                        .attachTo(node)
                    for ((resource, amount) in ability.refund) {
                        repeat(amount) {
                            assetManager.loadIcon(resource.icon)
                                .setLocalTranslation(offset, 0f)
                                .attachTo(node)
                            offset++
                        }
                    }
                    repeat(ability.restoreRandomSlot) {
                        // dice icon would be nice here
                        assetManager.loadIcon(Icon("Dungeon Crawl Stone Soup Full/misc/error.png"))
                            .setLocalTranslation(offset, 0f)
                            .attachTo(node)
                        offset++
                    }
                }
            }
        )
    }
}