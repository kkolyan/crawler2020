package ru.frogtail.onfoot.crawler2020.misc.gameloaders

import ru.frogtail.onfoot.crawler2020.game.state.Board
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.Person
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonBehavior
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.misc.collections.BiMap
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.GameLoader
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonAllocation
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonSlot
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector
import java.util.ArrayDeque

class TextBoardGameLoader(
    val background: Icon,
    val walls: Map<Char, Icon>,
    val hStep: Int = 1,
    val map: String
) : GameLoader {

    override fun load(persons: Map<PersonSlot, PersonAllocation>): Game {
        val lines = this.map.trim().lines().reversed()
        val width = lines.first().length / this.hStep + 1
        val height = lines.size
        val walls = mutableMapOf<GridVector, Icon>()
        val background = mutableMapOf<GridVector, Icon>()
        var nextPersonId = 1L
        val startLocations = mutableMapOf<PersonSlot, GridVector>()
        repeat(width) { west ->
            repeat(height) { south ->
                val north = height - south - 1
                val cellKey = GridVector(west, north)

                val line = lines.get(north).trim()
                val cellChar = line.get(west * this.hStep)

                val wall = this.walls.get(cellChar)
                val boardSlot = PersonSlot.ByChar.get(cellChar)

                if (boardSlot != null) {
                    startLocations.put(boardSlot, cellKey)
                }

                if (wall != null) {
                    walls.put(cellKey, wall)
                }
                background.put(cellKey, this.background)
            }
        }
        val personLocations = BiMap<GridVector, PersonId>()
        val personStates = mutableMapOf<PersonId, Person>()
        val turnQueue = ArrayDeque<PersonId>()
        val players = mutableMapOf<PersonId, PersonBehavior>()

        for ((slot, allocation) in persons) {
            val personDef = allocation.person
            val personKey = PersonId(nextPersonId++)
            val cellKey = startLocations.get(slot)
                ?: error("start location not found: $slot")
            personLocations.newLink(cellKey, personKey)
            personStates.put(personKey, Person(
                id = personKey,
                name = allocation.name,
                template = personDef,
                hp = personDef.maxHp,
                stacks = personDef.initialStacks.toMutableMap(),
                behavior = allocation.behavior
            ))
            players.put(personKey, allocation.behavior)
            turnQueue.addLast(personKey)
        }
        return Game(
            board = Board(
                width = width,
                height = height,
                background = background,
                walls = walls,
                foreground = mutableMapOf(),
                persons = personLocations
            ),
            persons = personStates,
            turnQueue = turnQueue
        )
    }
}