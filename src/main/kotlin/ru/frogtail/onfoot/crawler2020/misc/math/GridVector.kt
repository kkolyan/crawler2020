package ru.frogtail.onfoot.crawler2020.misc.math

import kotlin.math.pow
import kotlin.math.sqrt

data class GridVector(
    val east: Int,
    val north: Int
) {
    fun add(other: GridVector): GridVector {
        return GridVector(east + other.east, north + other.north)
    }

    fun subtract(other: GridVector): GridVector {
        return GridVector(east - other.east, north - other.north)
    }

    fun distanceTo(other: GridVector): Float {
        return sqrt((east - other.east).toFloat().pow(2) + (north - other.north).toFloat().pow(2))
    }

    fun length(): Float {
        return sqrt(east.toFloat().pow(2) + north.toFloat().pow(2))
    }
}