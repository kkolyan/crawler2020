package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.state.Game

class SceneDisplay(
    private val game: Game,
    private val assetManager: AssetManager
) {
    fun render(width: Float, height: Float): Spatial {
        val textNode = BitmapText(assetManager.loadDefaultFont())
        textNode.addUpdateControl {
            textNode.size = 0.3f
            textNode.setBox(Rectangle(
                0f,
                /*because text box is aligned by top*/
                height,
                width,
                height
            ))
            val info = game.display.info.map { "\\#FFF#$it" }
            val log = game.display.log
                .takeLast((height / textNode.lineHeight - info.size).toInt())
                .reversed()
                .map { "\\#FFF#${it.turn}. ${it.message}" }
            //https://hub.jmonkeyengine.org/t/bitmaptext-colortags-negativearraysizeexception/43357
            val workaroundJmeBug = "\\#FFFFFF#"

            textNode.text = workaroundJmeBug + (info + log)
                .joinToString("\n")
        }
        return textNode
    }
}