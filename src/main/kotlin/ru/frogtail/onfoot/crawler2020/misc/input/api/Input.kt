package ru.frogtail.onfoot.crawler2020.misc.input.api

interface Input {
    fun getButton(button: MouseButton): ButtonState
    fun getAxis(axis: Axis): Int
    fun getKey(key:KeyboardKey): KeyState
}