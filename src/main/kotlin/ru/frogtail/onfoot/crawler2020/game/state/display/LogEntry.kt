package ru.frogtail.onfoot.crawler2020.game.state.display

data class LogEntry(
    val turn: Long,
    val message: String,
    val tillMillis: Long
)