package ru.frogtail.onfoot.crawler2020.game.state.person

import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability

data class Person(
    val id: PersonId,
    val name: String,
    val template: PersonTemplate,
    var hp: Int,
    val stacks: MutableMap<ResourceSlot, ResourceStack>,
    val behavior: PersonBehavior,
    var selectedAbility: Ability? = null
)