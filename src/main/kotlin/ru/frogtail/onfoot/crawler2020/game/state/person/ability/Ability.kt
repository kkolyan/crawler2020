package ru.frogtail.onfoot.crawler2020.game.state.person.ability

import ru.frogtail.onfoot.crawler2020.game.data.Range
import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Icon

sealed class Ability {
    abstract val common: AbilityCommon

    val name: String get() = common.name
    val icon: Icon get() = common.icon
    val cost: Map<Resource, Int> get() = common.cost
    val refund: Map<Resource, Int> get() = common.refund
    val restoreRandomSlot: Int get() = common.restoreRandomSlot

    sealed class Place : Ability() {

        data class Jump(val range: Range, override val common: AbilityCommon) : Place()

        data class Charge(val range: Int, override val common: AbilityCommon) : Place()

        // throw explosive, for example
        data class AOE(
            val impact: StandardImpact,
            val range: Range,
            val radius: Range,
            val counterParty: CounterParty = CounterParty.Any,
            override val common: AbilityCommon
        ) : Place()
    }

    sealed class NoTarget : Ability() {
        data class AOE(
            val impact: StandardImpact,
            val radius: Range,
            val counterParty: CounterParty = CounterParty.Any,
            override val common: AbilityCommon
        ) : NoTarget()

        data class Self(
            val impact: StandardImpact,
            override val common: AbilityCommon
        ) : NoTarget()

        data class Stance(
            override val common: AbilityCommon
        ) : NoTarget()
    }

    sealed class Person : Ability() {
        abstract val counterParty: CounterParty

        data class Direct(
            override val counterParty: CounterParty,
            val range: Range,
            val impact: StandardImpact,
            override val common: AbilityCommon
        ) : Person()

    }
}