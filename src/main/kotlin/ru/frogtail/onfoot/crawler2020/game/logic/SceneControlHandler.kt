package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector

interface SceneControlHandler {
    fun hoverAbilityButton(ability: Ability)
    fun hoverTile(location: GridVector)
    fun hoverPerson(personId: PersonId)
}