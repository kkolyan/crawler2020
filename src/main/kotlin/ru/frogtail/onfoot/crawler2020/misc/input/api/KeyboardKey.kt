package ru.frogtail.onfoot.crawler2020.misc.input.api

import com.jme3.input.KeyInput
import com.jme3.input.controls.KeyTrigger
import com.jme3.input.controls.Trigger

enum class KeyboardKey(keyCode: Int) : Trigger {
    KEY_UNKNOWN(KeyInput.KEY_UNKNOWN),
    KEY_ESCAPE(KeyInput.KEY_ESCAPE),
    KEY_1(KeyInput.KEY_1),
    KEY_2(KeyInput.KEY_2),
    KEY_3(KeyInput.KEY_3),
    KEY_4(KeyInput.KEY_4),
    KEY_5(KeyInput.KEY_5),
    KEY_6(KeyInput.KEY_6),
    KEY_7(KeyInput.KEY_7),
    KEY_8(KeyInput.KEY_8),
    KEY_9(KeyInput.KEY_9),
    KEY_0(KeyInput.KEY_0),
    KEY_MINUS(KeyInput.KEY_MINUS),
    KEY_EQUALS(KeyInput.KEY_EQUALS),
    KEY_BACK(KeyInput.KEY_BACK),
    KEY_TAB(KeyInput.KEY_TAB),
    KEY_Q(KeyInput.KEY_Q),
    KEY_W(KeyInput.KEY_W),
    KEY_E(KeyInput.KEY_E),
    KEY_R(KeyInput.KEY_R),
    KEY_T(KeyInput.KEY_T),
    KEY_Y(KeyInput.KEY_Y),
    KEY_U(KeyInput.KEY_U),
    KEY_I(KeyInput.KEY_I),
    KEY_O(KeyInput.KEY_O),
    KEY_P(KeyInput.KEY_P),
    KEY_LBRACKET(KeyInput.KEY_LBRACKET),
    KEY_RBRACKET(KeyInput.KEY_RBRACKET),
    KEY_RETURN(KeyInput.KEY_RETURN),
    KEY_LCONTROL(KeyInput.KEY_LCONTROL),
    KEY_A(KeyInput.KEY_A),
    KEY_S(KeyInput.KEY_S),
    KEY_D(KeyInput.KEY_D),
    KEY_F(KeyInput.KEY_F),
    KEY_G(KeyInput.KEY_G),
    KEY_H(KeyInput.KEY_H),
    KEY_J(KeyInput.KEY_J),
    KEY_K(KeyInput.KEY_K),
    KEY_L(KeyInput.KEY_L),
    KEY_SEMICOLON(KeyInput.KEY_SEMICOLON),
    KEY_APOSTROPHE(KeyInput.KEY_APOSTROPHE),
    KEY_GRAVE(KeyInput.KEY_GRAVE),
    KEY_LSHIFT(KeyInput.KEY_LSHIFT),
    KEY_BACKSLASH(KeyInput.KEY_BACKSLASH),
    KEY_Z(KeyInput.KEY_Z),
    KEY_X(KeyInput.KEY_X),
    KEY_C(KeyInput.KEY_C),
    KEY_V(KeyInput.KEY_V),
    KEY_B(KeyInput.KEY_B),
    KEY_N(KeyInput.KEY_N),
    KEY_M(KeyInput.KEY_M),
    KEY_COMMA(KeyInput.KEY_COMMA),
    KEY_PERIOD(KeyInput.KEY_PERIOD),
    KEY_SLASH(KeyInput.KEY_SLASH),
    KEY_RSHIFT(KeyInput.KEY_RSHIFT),
    KEY_MULTIPLY(KeyInput.KEY_MULTIPLY),
    KEY_LMENU(KeyInput.KEY_LMENU),
    KEY_SPACE(KeyInput.KEY_SPACE),
    KEY_CAPITAL(KeyInput.KEY_CAPITAL),
    KEY_F1(KeyInput.KEY_F1),
    KEY_F2(KeyInput.KEY_F2),
    KEY_F3(KeyInput.KEY_F3),
    KEY_F4(KeyInput.KEY_F4),
    KEY_F5(KeyInput.KEY_F5),
    KEY_F6(KeyInput.KEY_F6),
    KEY_F7(KeyInput.KEY_F7),
    KEY_F8(KeyInput.KEY_F8),
    KEY_F9(KeyInput.KEY_F9),
    KEY_F10(KeyInput.KEY_F10),
    KEY_NUMLOCK(KeyInput.KEY_NUMLOCK),
    KEY_SCROLL(KeyInput.KEY_SCROLL),
    KEY_NUMPAD7(KeyInput.KEY_NUMPAD7),
    KEY_NUMPAD8(KeyInput.KEY_NUMPAD8),
    KEY_NUMPAD9(KeyInput.KEY_NUMPAD9),
    KEY_SUBTRACT(KeyInput.KEY_SUBTRACT),
    KEY_NUMPAD4(KeyInput.KEY_NUMPAD4),
    KEY_NUMPAD5(KeyInput.KEY_NUMPAD5),
    KEY_NUMPAD6(KeyInput.KEY_NUMPAD6),
    KEY_ADD(KeyInput.KEY_ADD),
    KEY_NUMPAD1(KeyInput.KEY_NUMPAD1),
    KEY_NUMPAD2(KeyInput.KEY_NUMPAD2),
    KEY_NUMPAD3(KeyInput.KEY_NUMPAD3),
    KEY_NUMPAD0(KeyInput.KEY_NUMPAD0),
    KEY_DECIMAL(KeyInput.KEY_DECIMAL),
    KEY_F11(KeyInput.KEY_F11),
    KEY_F12(KeyInput.KEY_F12),
    KEY_F13(KeyInput.KEY_F13),
    KEY_F14(KeyInput.KEY_F14),
    KEY_F15(KeyInput.KEY_F15),
    KEY_KANA(KeyInput.KEY_KANA),
    KEY_CONVERT(KeyInput.KEY_CONVERT),
    KEY_NOCONVERT(KeyInput.KEY_NOCONVERT),
    KEY_YEN(KeyInput.KEY_YEN),
    KEY_NUMPADEQUALS(KeyInput.KEY_NUMPADEQUALS),
    KEY_CIRCUMFLEX(KeyInput.KEY_CIRCUMFLEX),
    KEY_AT(KeyInput.KEY_AT),
    KEY_COLON(KeyInput.KEY_COLON),
    KEY_UNDERLINE(KeyInput.KEY_UNDERLINE),
    KEY_KANJI(KeyInput.KEY_KANJI),
    KEY_STOP(KeyInput.KEY_STOP),
    KEY_AX(KeyInput.KEY_AX),
    KEY_UNLABELED(KeyInput.KEY_UNLABELED),
    KEY_PRTSCR(KeyInput.KEY_PRTSCR),
    KEY_NUMPADENTER(KeyInput.KEY_NUMPADENTER),
    KEY_RCONTROL(KeyInput.KEY_RCONTROL),
    KEY_NUMPADCOMMA(KeyInput.KEY_NUMPADCOMMA),
    KEY_DIVIDE(KeyInput.KEY_DIVIDE),
    KEY_SYSRQ(KeyInput.KEY_SYSRQ),
    KEY_RMENU(KeyInput.KEY_RMENU),
    KEY_PAUSE(KeyInput.KEY_PAUSE),
    KEY_HOME(KeyInput.KEY_HOME),
    KEY_UP(KeyInput.KEY_UP),
    KEY_PRIOR(KeyInput.KEY_PRIOR),
    KEY_PGUP(KeyInput.KEY_PGUP),
    KEY_LEFT(KeyInput.KEY_LEFT),
    KEY_RIGHT(KeyInput.KEY_RIGHT),
    KEY_END(KeyInput.KEY_END),
    KEY_DOWN(KeyInput.KEY_DOWN),
    KEY_NEXT(KeyInput.KEY_NEXT),
    KEY_PGDN(KeyInput.KEY_PGDN),
    KEY_INSERT(KeyInput.KEY_INSERT),
    KEY_DELETE(KeyInput.KEY_DELETE),
    KEY_LMETA(KeyInput.KEY_LMETA),
    KEY_RMETA(KeyInput.KEY_RMETA),
    KEY_APPS(KeyInput.KEY_APPS),
    KEY_POWER(KeyInput.KEY_POWER),
    KEY_SLEEP(KeyInput.KEY_SLEEP),
    KEY_LAST(KeyInput.KEY_LAST),
    ;

    val trigger = KeyTrigger(keyCode)

    override fun triggerHashCode() = trigger.triggerHashCode()

    override fun getName(): String = trigger.name

    companion object {
        val ByCode = values().associateBy { it.trigger.keyCode }

        fun numberKey(number: Int): KeyboardKey {
            return when (number) {
                1 -> KEY_1
                2 -> KEY_2
                3 -> KEY_3
                4 -> KEY_4
                5 -> KEY_5
                6 -> KEY_6
                7 -> KEY_7
                8 -> KEY_8
                9 -> KEY_9
                0 -> KEY_0
                else -> error("no such number key: $number")
            }
        }
    }
}