package ru.frogtail.onfoot.crawler2020.misc.gamrunner

import ru.frogtail.onfoot.crawler2020.game.logic.SystemsRegistry
import ru.frogtail.onfoot.crawler2020.game.visual.RenderersRegistry
import ru.frogtail.onfoot.crawler2020.misc.gameloaders.LoadableArea

object GameLauncher {
    fun startGame(area: LoadableArea, persons: Map<PersonSlot, PersonAllocation>) {
        val game = area.loader.load(persons)
        val runner = GenericGameRunner(game)
        RenderersRegistry.populateRenderers(runner::addSystem)
        SystemsRegistry.populateSystems(runner::addSystem)
        runner.start()
    }
}