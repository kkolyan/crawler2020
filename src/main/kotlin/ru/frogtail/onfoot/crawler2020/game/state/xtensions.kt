package ru.frogtail.onfoot.crawler2020.game.state

import ru.frogtail.onfoot.crawler2020.game.state.display.LogEntry
import ru.frogtail.onfoot.crawler2020.game.state.person.Person

fun Game.resolveTurnOwner(): Person? {
    val personId = turnQueue.firstOrNull()
    return persons.get(personId)
}

inline fun <reified T> Any.asIf(): T? {
    return this as? T
}

fun Any.color(color: String): String {
    return "\\#$color#$this\\#FFF#"
}

fun Game.log(message: String, ttlMillis: Long = Long.MAX_VALUE / 2) {
    display.log.add(LogEntry(turn, message, System.currentTimeMillis() + ttlMillis))
}

fun Game.info(line: String) {
    display.info.add(line)
}