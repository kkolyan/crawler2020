package ru.frogtail.onfoot.crawler2020.game.data.abilities

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityCommon

object Stance {

    val DefenceStance = Ability.NoTarget.Stance(AbilityCommon(
        name = "Defence Stance",
        icon = Icon("Dungeon Crawl Stone Soup Full/gui/skills/shields.png"),
        cost = mapOf(Resource.MeleeAttack to 1),
        refund = mapOf(Resource.Block to 1)
    ))

    val ReadyStance = Ability.NoTarget.Stance(AbilityCommon(
        name = "Ready Stance",
        icon = Icon("Dungeon Crawl Stone Soup Full/gui/skills/conjurations.png"),
        cost = mapOf(Resource.MeleeAttack to 1),
        refund = mapOf(Resource.Speed to 1)
    ))

    val MissileStance = Ability.NoTarget.Stance(AbilityCommon(
        name = "Missile Stance",
        icon = Icon("Dungeon Crawl Stone Soup Full/gui/skills/spellcasting.png"),
        cost = mapOf(Resource.MeleeAttack to 1),
        refund = mapOf(Resource.MissileAttack to 1)
    ))

    val TakeABreak = Ability.NoTarget.Stance(AbilityCommon(
        name = "Take a break",
        icon = Icon("np/ability-rest.png"),
        restoreRandomSlot = 1
    ))
}