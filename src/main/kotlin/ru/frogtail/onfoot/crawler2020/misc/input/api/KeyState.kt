package ru.frogtail.onfoot.crawler2020.misc.input.api

interface KeyState : ButtonState {
    val repeating: Boolean
}