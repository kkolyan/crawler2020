package ru.frogtail.onfoot.crawler2020.game.visual

import kotlin.reflect.KClass

object RenderersRegistry {
    fun populateRenderers(register: (KClass<*>) -> Unit) {
        register(SceneRootFactory::class)
        register(SceneLayout::class)
        register(SceneAbilitySelectBar::class)
        register(SceneBoard::class)
        register(SceneDisplay::class)
        register(SceneCursor::class)
        register(ScenePerson::class)
        register(SceneAbilityInfoPanel::class)
        register(ScenePersonInfoPanel::class)
    }
}