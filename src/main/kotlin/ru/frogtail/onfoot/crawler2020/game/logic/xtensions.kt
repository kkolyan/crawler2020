package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.state.person.Person
import ru.frogtail.onfoot.crawler2020.game.state.person.ResourceStack
import kotlin.math.max
import kotlin.math.min


fun Person.withdrawResource(resource: Resource, amount: Int) {
    check(amount >= 0)
    val stack = stacks.get(resource.slot)
    check(stack != null && stack.resource == resource)
    val newSize = max(0, stack.size - amount)
    stacks[resource.slot] = ResourceStack(resource, newSize)
}

fun Person.addToSlot(slot: ResourceSlot, amount: Int) {
    check(amount >= 0)
    val stack = stacks.getValue(slot)
    addResource(stack.resource, amount)
}

fun Person.addResource(resource: Resource, amount: Int) {
    check(amount >= 0)
    val stack = stacks.getValue(resource.slot)
    val capacity = template.initialStacks.get(resource.slot)?.size ?: 0
    stacks[resource.slot] = ResourceStack(resource, max(stack.size, min(capacity, stack.size + amount)))
}
