package ru.frogtail.onfoot.crawler2020.game.data.persons

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Attack
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Movement
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Stance
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Support
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.DefenceReaction
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonTemplate
import ru.frogtail.onfoot.crawler2020.game.state.person.ResourceStack
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonTemplateHolder

object Knight : PersonTemplateHolder {
    override val template = PersonTemplate(
        name = "Knight",
        icon = Icon("Dungeon Crawl Stone Soup Full/monster/unique/norbert.png"),
        maxHp = 20,
        abilities = listOf(
            Stance.TakeABreak,
            Stance.DefenceStance,
            Movement.Walk,
            Attack.AttackNormal,
            Support.Heal
        ),
        initialStacks = mapOf(
            ResourceSlot.PrimaryAttack to ResourceStack(Resource.MeleeAttack, 2),
            ResourceSlot.Offhand to ResourceStack(Resource.Block, 2),
            ResourceSlot.Mobility to ResourceStack(Resource.Dodge, 1),
            ResourceSlot.Concentration to ResourceStack(Resource.Tactic, 1),
            ResourceSlot.MagicSlot0 to ResourceStack(Resource.MagicRune01, 2)
        ),
        defences = listOf(
            DefenceReaction(Resource.Dodge),
            DefenceReaction(Resource.Parry),
            DefenceReaction(Resource.Block)
        )
    )
}