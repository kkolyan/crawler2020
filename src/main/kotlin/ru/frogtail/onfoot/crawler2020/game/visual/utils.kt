package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.scene.Node
import com.jme3.scene.Spatial


fun <S> createReactiveNode(name: String, resolveState: () -> S, renderContent: (node: Node, state: S) -> Unit): Spatial {
    val node = Node(name)
    var prevHashCode = 0
    node.addUpdateControl {
        val actualState = resolveState()
        if (actualState.hashCode() != prevHashCode) {
            node.detachAllChildren()
            renderContent(node, actualState)
            prevHashCode = actualState.hashCode()
        }
    }
    return node
}