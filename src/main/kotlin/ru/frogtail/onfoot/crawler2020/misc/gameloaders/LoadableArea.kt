package ru.frogtail.onfoot.crawler2020.misc.gameloaders

import ru.frogtail.onfoot.crawler2020.misc.gamrunner.GameLoader

interface LoadableArea {
    val loader: GameLoader
}