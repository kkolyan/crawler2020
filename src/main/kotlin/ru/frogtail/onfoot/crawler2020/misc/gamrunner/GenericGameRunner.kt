package ru.frogtail.onfoot.crawler2020.misc.gamrunner

import com.jme3.app.SimpleApplication
import com.jme3.scene.Node
import com.jme3.system.AppSettings
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.support.GenericApplicationContext
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.visual.addUpdateControl
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.integration.GuiRenderer
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.integration.Lifecycle
import ru.frogtail.onfoot.crawler2020.misc.input.InputProcessor
import kotlin.reflect.KClass

@Suppress("RemoveRedundantSpreadOperator") // to avoid default constructor with default pack of states
class GenericGameRunner(private val game: Game) : SimpleApplication(*arrayOf()) {
    init {
        isShowSettings = false
        settings = AppSettings(true)
        settings.isResizable = true
        settings.frameRate = 60
        settings.width = 1366
        settings.height = 768
        settings.isFullscreen = false
    }

    private var applicationContext: AnnotationConfigApplicationContext? = null
    private val beforeInput = mutableListOf<() -> Unit>()
    private val beforeUpdate = Node("Before Update")
    private val afterUpdate = Node("After Update")

    private val systems = mutableSetOf<KClass<*>>()

    fun addSystem(kClass: KClass<*>) {
        systems.add(kClass)
    }

    override fun simpleInitApp() {
        setDisplayFps(true)
        setDisplayStatView(false)

        createApplicationContext()
        guiNode.attachChild(beforeUpdate)
        guiNode.attachChild(applicationContext!!.getBean(GuiRenderer::class.java).render())
        guiNode.attachChild(afterUpdate)
    }

    private fun createApplicationContext() {
        val parent = GenericApplicationContext()

        val input = InputProcessor(
            guiNode = guiNode,
            beforeInput = {
                beforeInput.forEach { it() }
            }
        )
        inputManager.clearMappings()
        inputManager.addRawInputListener(input)
        parent.defaultListableBeanFactory.registerSingleton("input", input)

        parent.defaultListableBeanFactory.registerSingleton("assetManager", assetManager)
        parent.defaultListableBeanFactory.registerSingleton("game", game)

        val applicationContext = AnnotationConfigApplicationContext(parent.defaultListableBeanFactory)
        for (system in systems) {
            applicationContext.register(system.java)
        }
        applicationContext.refresh()

        val integration = applicationContext.getBean(Lifecycle::class.java)
        beforeInput.add {
            integration.beforeInput()
        }
        beforeUpdate.addUpdateControl { integration.beforeUpdate() }
        afterUpdate.addUpdateControl {
            integration.afterUpdate()
            if (integration.isQuit()) {
                stop()
            }
        }

        this.applicationContext = applicationContext
    }

    override fun stop() {
        super.stop()
        applicationContext!!.stop()
    }
}