package ru.frogtail.onfoot.crawler2020.misc.input

import com.jme3.collision.CollisionResult
import com.jme3.collision.CollisionResults
import com.jme3.input.RawInputListener
import com.jme3.input.event.JoyAxisEvent
import com.jme3.input.event.JoyButtonEvent
import com.jme3.input.event.KeyInputEvent
import com.jme3.input.event.MouseButtonEvent
import com.jme3.input.event.MouseMotionEvent
import com.jme3.input.event.TouchEvent
import com.jme3.math.Ray
import com.jme3.math.Vector3f
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.misc.input.api.Axis
import ru.frogtail.onfoot.crawler2020.misc.input.api.ButtonState
import ru.frogtail.onfoot.crawler2020.misc.input.api.Input
import ru.frogtail.onfoot.crawler2020.misc.input.api.KeyState
import ru.frogtail.onfoot.crawler2020.misc.input.api.KeyboardKey
import ru.frogtail.onfoot.crawler2020.misc.input.api.MouseButton
import ru.frogtail.onfoot.crawler2020.misc.input.ui.HoverControl
import java.util.EnumMap

class InputProcessor(
    private val guiNode: Node,
    private val beforeInput: () -> Unit = {}
) : RawInputListener, Input {

    private val axes = IntArray(Axis.values().size)
    private val mouseButtons = EnumMap<MouseButton, EditableMouseButtonState>(MouseButton::class.java)
    private val keyboardKeys = EnumMap<KeyboardKey, EditableKeyState>(KeyboardKey::class.java)

    override fun getKey(key: KeyboardKey): KeyState = keyboardKeys.get(key) ?: EmptyKeyState
    override fun getButton(button: MouseButton): ButtonState = mouseButtons.get(button) ?: EmptyKeyState
    override fun getAxis(axis: Axis): Int = axes.get(axis.ordinal)

    init {
        axes[Axis.MOUSE_X.ordinal] = -1
        axes[Axis.MOUSE_Y.ordinal] = -1
    }

    override fun beginInput() {
        beforeInput()

        for (value in mouseButtons.values) {
            value.justPressed = false
            value.justReleased = false
        }
        for (value in keyboardKeys.values) {
            value.justPressed = false
            value.justReleased = false
            value.repeating = false
        }
    }

    override fun endInput() {
        val mx = getAxis(Axis.MOUSE_X).toFloat()
        val my = getAxis(Axis.MOUSE_Y).toFloat()
        val noDistanceResults = object : CollisionResults() {
            override fun addCollision(result: CollisionResult) {
                super.addCollision(CollisionResult(result.geometry, result.contactPoint, 0f, result.triangleIndex))
            }
        }
        guiNode.collideWith(Ray(Vector3f(mx, my, -1f), Vector3f(0f, 0f, 1f)), noDistanceResults)
        for (result in noDistanceResults.reversed()) {
            var spatial : Spatial? = result?.geometry
            while (spatial != null) {
                for (i in 0 until spatial.numControls) {
                    val hoverHandler = spatial.getControl(i)
                    if (hoverHandler is HoverControl) {
                        hoverHandler.oHover()
                    }
                }
                spatial = spatial.parent
            }
        }
    }

    override fun onJoyAxisEvent(evt: JoyAxisEvent) {
    }

    override fun onTouchEvent(evt: TouchEvent) {
    }

    override fun onMouseButtonEvent(evt: MouseButtonEvent) {
        val button = MouseButton.ByCode.get(evt.buttonIndex)
        if (button != null) {
            val state = mouseButtons.getOrPut(button) { EditableMouseButtonState() }
            state.justReleased = !evt.isPressed
            state.justPressed = evt.isPressed
            state.pressing = evt.isPressed
        }
    }

    override fun onMouseMotionEvent(evt: MouseMotionEvent) {
        axes[Axis.MOUSE_X.ordinal] = evt.x
        axes[Axis.MOUSE_Y.ordinal] = evt.y
        axes[Axis.MOUSE_DELTA_X.ordinal] = evt.dx
        axes[Axis.MOUSE_DELTA_Y.ordinal] = evt.dy
        axes[Axis.MOUSE_WHEEL.ordinal] = evt.wheel
        axes[Axis.MOUSE_DELTA_WHEEL.ordinal] = evt.deltaWheel
    }

    override fun onKeyEvent(evt: KeyInputEvent) {
        val key = KeyboardKey.ByCode.get(evt.keyCode)
        if (key != null) {
            val state = keyboardKeys.getOrPut(key = key) { EditableKeyState() }
            state.justReleased = !evt.isPressed
            state.justPressed = evt.isPressed
            state.pressing = evt.isPressed
            state.repeating = evt.isRepeating
        }
    }

    override fun onJoyButtonEvent(evt: JoyButtonEvent) {
    }

    private abstract class EditableButtonState : ButtonState {
        override var justReleased: Boolean = false
        override var justPressed: Boolean = false
        override var pressing: Boolean = false
    }

    private class EditableMouseButtonState : EditableButtonState() {
    }

    private class EditableKeyState : EditableButtonState(), KeyState {
        override var repeating: Boolean = false
    }

    private object EmptyKeyState: KeyState {
        override val repeating: Boolean = false
        override val justReleased: Boolean = false
        override val justPressed: Boolean = false
        override val pressing: Boolean = false
    }
}