package ru.frogtail.onfoot.crawler2020.game.data.abilities

import ru.frogtail.onfoot.crawler2020.game.data.Range
import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityCommon

object Movement {
    val Jump = Ability.Place.Jump(Range.VeryFar, common = AbilityCommon(
        name = "Jump",
        icon = Icon("np/ability-jump.png"),
        cost = mapOf(Resource.Trick to 1)
    ))

    val Walk = Ability.Place.Charge(
        range = 1,
        common = AbilityCommon(
            name = "Walk",
            icon = Icon("np/ability-walk.png")
        )
    )

    val Charge = Ability.Place.Charge(
        range = 5,
        common = AbilityCommon(
            name = "Charge",
            icon = Icon("np/ability-charge.png"),
            cost = mapOf(Resource.Speed to 1)
        )
    )
}