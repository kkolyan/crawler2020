package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.logic.SceneControlHandler
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.misc.input.ui.addHoverControl
import ru.frogtail.onfoot.crawler2020.misc.scene.Icons

class ScenePerson(
    private val game: Game,
    private val assetManager: AssetManager,
    private val controlHandler: SceneControlHandler
) {
    fun render(id: PersonId): Spatial {
        val sprite = Icons.createPicture(assetManager, game.persons.getValue(id).template.icon)
        sprite.addHoverControl { controlHandler.hoverPerson(id) }
        sprite.addUpdateControl {
            val place = game.board.persons.getA(id)
            if (place != null) {
                sprite.setLocalTranslation(place)
            }
        }
        return sprite
    }
}