package ru.frogtail.onfoot.crawler2020.game.state.person

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability


fun Person.getResource(resource: Resource): Int {
    val stack = stacks.get(resource.slot)
    if (stack == null || stack.resource != resource) {
        return 0
    }
    return stack.size
}

fun Person.calculateCostLack(ability: Ability): Map<Resource, Int> {
    return ability.cost
        .mapValues { (resource, value) ->
            val available = getResource(resource)
            value - available
        }
        .filter { it.value > 0 }
}
