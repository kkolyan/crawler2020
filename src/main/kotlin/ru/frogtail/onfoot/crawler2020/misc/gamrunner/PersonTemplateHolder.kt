package ru.frogtail.onfoot.crawler2020.misc.gamrunner

import ru.frogtail.onfoot.crawler2020.game.state.person.PersonTemplate

interface PersonTemplateHolder {
    val template: PersonTemplate
}