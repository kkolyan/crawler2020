package ru.frogtail.onfoot.crawler2020.game.data.persons

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Support
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.DefenceReaction
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonTemplate
import ru.frogtail.onfoot.crawler2020.game.state.person.ResourceStack
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonTemplateHolder

object Norris : PersonTemplateHolder {
    override val template = PersonTemplate(
        name = "Norris",
        icon = Icon("Dungeon Crawl Stone Soup Full/monster/unique/norris.png"),
        maxHp = 100,
        abilities = listOf(
            Support.DivineIntervention
        ),
        initialStacks = mapOf(
            ResourceSlot.PrimaryAttack to ResourceStack(Resource.MeleeAttack, 2),
            ResourceSlot.Offhand to ResourceStack(Resource.Block, 2),
            ResourceSlot.Mobility to ResourceStack(Resource.Dodge, 2),
            ResourceSlot.Concentration to ResourceStack(Resource.Tactic, 1)
        ),
        defences = listOf(
            DefenceReaction(Resource.Dodge),
            DefenceReaction(Resource.Parry),
            DefenceReaction(Resource.Block)
        )
    )
}