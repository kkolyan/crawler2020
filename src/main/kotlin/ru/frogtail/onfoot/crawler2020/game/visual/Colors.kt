package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.math.ColorRGBA

object Colors {
    // got from com\jme3\font\ColorTags.java
    fun parseColor(colorStr: String, baseAlpha: Float = -1f): ColorRGBA {
        val color = ColorRGBA()
        if (colorStr.length >= 6) {
            color.set(colorStr.subSequence(0, 2).toString().toInt(16) / 255f,
                colorStr.subSequence(2, 4).toString().toInt(16) / 255f,
                colorStr.subSequence(4, 6).toString().toInt(16) / 255f, 1f)
            if (baseAlpha != -1f) {
                color.a = baseAlpha
            } else if (colorStr.length == 8) {
                color.a = colorStr.subSequence(6, 8).toString().toInt(16) / 255f
            }
        } else {
            color.set(Character.toString(colorStr.get(0)).toInt(16) / 15f,
                Character.toString(colorStr.get(1)).toInt(16) / 15f,
                Character.toString(colorStr.get(2)).toInt(16) / 15f, 1f)
            if (baseAlpha != -1f) {
                color.a = baseAlpha
            } else if (colorStr.length == 4) {
                color.a = Character.toString(colorStr.get(3)).toInt(16) / 15f
            }
        }
        return color
    }
}