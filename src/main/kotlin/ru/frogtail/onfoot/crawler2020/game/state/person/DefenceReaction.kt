package ru.frogtail.onfoot.crawler2020.game.state.person

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.BonusOnDrain

data class DefenceReaction(
    val resource: Resource,
    val onDrain: BonusOnDrain? = null
)