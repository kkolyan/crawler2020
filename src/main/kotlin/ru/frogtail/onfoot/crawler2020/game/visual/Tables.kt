package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.scene.Node
import com.jme3.scene.Spatial

object Tables {

    fun renderTable(assetManager: AssetManager, data: Map<String, String>): Spatial {
        val node = Node("Table")
        val rowH = 0.5f
        val labelColumnW = 2f
        val valueColumnX = 2.125f
        val valueColumnW = 4f
        val fontSize = 0.3f

        var i = 0
        for ((label, value) in data) {
            BitmapText(assetManager.loadDefaultFont())
                .also {
                    it.text = label
                    it.size = fontSize
                    it.setBox(Rectangle(0f, (data.size - i) * rowH, labelColumnW, rowH))
                    it.alignment = BitmapFont.Align.Right
                    it.verticalAlignment = BitmapFont.VAlign.Center
                }
                .attachTo(node)
            BitmapText(assetManager.loadDefaultFont())
                .also {
                    it.text = value
                    it.size = fontSize
                    it.setBox(Rectangle(valueColumnX, (data.size - i) * rowH, valueColumnW, rowH))
                    it.alignment = BitmapFont.Align.Left
                    it.verticalAlignment = BitmapFont.VAlign.Center
                }
                .attachTo(node)
            i++
        }
        return node
    }
}