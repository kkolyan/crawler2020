package ru.frogtail.onfoot.crawler2020.misc.input.api

interface ButtonState {
    val justReleased: Boolean
    val justPressed: Boolean
    val pressing: Boolean
}