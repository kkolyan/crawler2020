package ru.frogtail.onfoot.crawler2020.game.data

enum class ResourceSlot {
    PrimaryAttack,
    Offhand,
    Mobility,
    Concentration,
    MagicSlot0,
}