package ru.frogtail.onfoot.crawler2020.game.logic

import kotlin.reflect.KClass

object SystemsRegistry {
    fun populateSystems(register: (KClass<*>) -> Unit) {
        register(ControlSystem::class)
        register(AISystem::class)
        register(LifecycleSystem::class)
        register(AbilitySystem::class)
        register(ImpactSystem::class)
        register(DisplaySystem::class)
        register(TurnSystem::class)
    }
}