package ru.frogtail.onfoot.crawler2020.game.state.person.ability

import ru.frogtail.onfoot.crawler2020.game.data.Resource

data class BonusOnDrain(
    val resource: Resource,
    val amount: Int
)