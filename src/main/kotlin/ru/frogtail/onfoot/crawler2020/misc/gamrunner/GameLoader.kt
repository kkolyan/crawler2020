package ru.frogtail.onfoot.crawler2020.misc.gamrunner

import ru.frogtail.onfoot.crawler2020.game.state.Game

interface GameLoader {
    fun load(persons: Map<PersonSlot, PersonAllocation>): Game
}