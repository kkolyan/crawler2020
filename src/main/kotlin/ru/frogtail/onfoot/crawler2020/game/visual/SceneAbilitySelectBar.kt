package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.logic.SceneControlHandler
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.ContextualIntent
import ru.frogtail.onfoot.crawler2020.game.state.person.calculateCostLack
import ru.frogtail.onfoot.crawler2020.game.state.resolveTurnOwner
import ru.frogtail.onfoot.crawler2020.misc.input.ui.addHoverControl
import ru.frogtail.onfoot.crawler2020.misc.scene.Icons

class SceneAbilitySelectBar(
    private val game: Game,
    private val assetManager: AssetManager,
    private val controlHandler: SceneControlHandler
) {
    fun render(): Spatial {
        val node = Node("Abilities")
        val buttonsPanel = Node("Buttons")
        node.attachChild(buttonsPanel)
        val buttons = mutableMapOf<Ability, AbilityButton>()
        val visible = mutableListOf<Ability>()
        node.addUpdateControl {
            val turnOwnerId = game.turnQueue.firstOrNull()
            val turnOwner = game.persons.get(turnOwnerId)
            if (turnOwner?.template?.abilities != visible) {
                buttonsPanel.detachAllChildren()
                visible.clear()

                for ((i, ability) in turnOwner?.template?.abilities.orEmpty().withIndex()) {
                    visible.add(ability)
                    val button = buttons.computeIfAbsent(ability) {
                        AbilityButton(assetManager, it, controlHandler, game)
                    }
                    buttonsPanel.attachChild(button)
                    button.setLocalTranslation(i.toFloat(), 0f)
                }
            }
        }
        return node
    }

    private class AbilityButton(
        assetManager: AssetManager,
        ability: Ability,
        controlHandler: SceneControlHandler,
        game: Game
    ) : Node("Ability Button $ability") {
        init {
            addHoverControl { controlHandler.hoverAbilityButton(ability) }

            val backgroundNode = Node("Background");
            attachChild(backgroundNode)

            val picture = Icons.createPicture(assetManager, ability.icon)
            picture.addUpdateControl {

                val actor = game.resolveTurnOwner()
                if (actor != null) {
                    val resourcesLack = actor.calculateCostLack(ability)
                    picture.material.setColor("Color", when {
                        resourcesLack.isEmpty() -> ColorRGBA.Black
                        else -> ColorRGBA.Gray
                    })
                }
            }
            attachChild(picture)

            val normalBackground = Icons.createPicture(assetManager, Icon("Dungeon Crawl Stone Soup Full/gui/skills/enabled-base.png"))
            val selectedBackground = Icons.createPicture(assetManager, Icon("Dungeon Crawl Stone Soup Full/gui/skills/enabled-fg.png"))
            val autoSelectedBackground = Icons.createPicture(assetManager, Icon("Dungeon Crawl Stone Soup Full/gui/skills/focused-fg.png"))

            var background: Spatial? = null

            addUpdateControl {
                val actor = game.resolveTurnOwner()
                val action = game.intent
                val desiredBackground = when {
                    actor != null && actor.selectedAbility == ability -> selectedBackground
                    actor != null && action != null && action is ContextualIntent.Auto && action.intent.ability == ability -> autoSelectedBackground
                    else -> normalBackground
                }
                if (desiredBackground != background) {
                    if (background != null) {
                        backgroundNode.detachChild(background)
                    }
                    backgroundNode.attachChild(desiredBackground)
                    background = desiredBackground
                }
            }
        }
    }
}