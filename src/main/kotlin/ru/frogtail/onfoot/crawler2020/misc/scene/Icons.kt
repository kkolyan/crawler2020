package ru.frogtail.onfoot.crawler2020.misc.scene

import com.jme3.asset.AssetManager
import com.jme3.asset.TextureKey
import com.jme3.math.ColorRGBA
import com.jme3.texture.Texture
import com.jme3.texture.Texture2D
import com.jme3.ui.Picture
import ru.frogtail.onfoot.crawler2020.game.state.Icon

object Icons {
    fun createPicture(assetManager: AssetManager, icon: Icon, color: ColorRGBA? = null) : Picture {
        val picture = Picture("Icon $icon")

        val key = TextureKey(icon.path, true)
        val tex = assetManager.loadTexture(key) as Texture2D
        tex.magFilter = Texture.MagFilter.Nearest
        picture.setTexture(assetManager, tex, true)
        if (color != null) {
            picture.material.setColor("Color", color)
        }
        return picture
    }
}