package ru.frogtail.onfoot.crawler2020.game.state.person.ability

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Icon

data class AbilityCommon(
    val name: String,
    val icon: Icon,
    val cost: Map<Resource, Int> = mapOf(),
    val refund: Map<Resource, Int> = mapOf(),
    val restoreRandomSlot: Int = 0
)