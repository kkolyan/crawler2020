package ru.frogtail.onfoot.crawler2020.misc.gamrunner.integration

interface Lifecycle {
    fun beforeInput()
    fun beforeUpdate()
    fun afterUpdate()
    fun isQuit(): Boolean
}