package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.scene.Spatial
import org.lwjgl.opengl.Display
import ru.frogtail.onfoot.crawler2020.game.data.LayoutConstants
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.integration.GuiRenderer

class SceneRootFactory(
    private val sceneLayout: SceneLayout
) : GuiRenderer {
    override fun render(): Spatial {
        val node = sceneLayout.render()

        val adjustSize = {
            val h = Display.getHeight()
            val ratio = 1f * h / LayoutConstants.LogicalScreenHeight
            node.setLocalScale(ratio)
        }
        adjustSize()
        node.addUpdateControl {
            if (Display.wasResized()) {
                adjustSize()
            }
        }

        return node
    }
}