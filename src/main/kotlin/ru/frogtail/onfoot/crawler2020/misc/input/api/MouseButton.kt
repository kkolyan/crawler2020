package ru.frogtail.onfoot.crawler2020.misc.input.api

import com.jme3.input.MouseInput
import com.jme3.input.controls.MouseButtonTrigger
import com.jme3.input.controls.Trigger

enum class MouseButton(buttonCode: Int) : Trigger {
    LEFT(MouseInput.BUTTON_LEFT),
    MIDDLE(MouseInput.BUTTON_MIDDLE),
    RIGHT(MouseInput.BUTTON_RIGHT),
    ;

    private val trigger = MouseButtonTrigger(buttonCode)

    override fun triggerHashCode() = trigger.triggerHashCode()

    override fun getName(): String = trigger.name

    companion object {
        val ByCode = values().associateBy { it.trigger.mouseButton }
    }
}