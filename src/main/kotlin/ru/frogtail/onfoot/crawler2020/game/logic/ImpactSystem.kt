package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.color
import ru.frogtail.onfoot.crawler2020.game.state.log
import ru.frogtail.onfoot.crawler2020.game.state.person.Person
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.BonusOnDrain
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.StandardImpact
import ru.frogtail.onfoot.crawler2020.game.state.person.getResource
import kotlin.math.max
import kotlin.math.min

@Suppress("SuspiciousCollectionReassignment")
class ImpactSystem(private val game: Game) {

    fun acceptImpact(actor: Person, impact: StandardImpact, target: Person) {
        val defence = selectDefence(target, impact)
        if (defence == null) {
            applyImpact(actor, impact, target)
        } else {
            applyDefence(target, defence, actor)
        }
    }

    fun beforeStartTurn(actor: Person) {
        // NOTE: it is not called before first turn. will fix if necessary
    }

    fun beforeEndTurn(actor: Person) {
        for (personId in game.turnQueue.toList()) {
            val person = game.persons.getValue(personId)
            if (person.hp <= 0) {
                game.turnQueue.remove(person.id)
                game.board.persons.unlinkB(person.id)
                game.log("${person.name.color("FF0")} ${"died".color("F77")}")
            }
        }
    }

    private fun applyDefence(target: Person, defence: Defence, actor: Person) {
        target.withdrawResource(defence.resource, defence.cost)
        if (target.getResource(defence.resource) <= 0 && defence.onDrain != null) {
            target.addResource(defence.onDrain.resource, defence.onDrain.amount)
        }
        game.log("${target.name.color("FF0")} defends by ${defence.resource} from ${actor.name.color("FF0")}")
    }

    private fun applyImpact(actor: Person, impact: StandardImpact, target: Person) {
        if (impact.damage != null) {
            target.hp -= impact.damage
            if (actor == target) {
                game.log("${actor.name.color("FF0")} hits self for ${impact.damage.color("F77")}")
            } else {
                game.log("${actor.name.color("FF0")} hits ${target.name.color("FF0")} for ${impact.damage.color("F77")}")
            }
        }
        if (impact.heal != null) {
            val amount = max(0, min(impact.heal, target.template.maxHp - target.hp))
            target.hp += amount
            if (actor == target) {
                game.log("${actor.name.color("FF0")} heals self for ${amount.color("0F0")}")
            } else {
                game.log("${actor.name.color("FF0")} heals ${target.name.color("FF0")} for ${amount.color("0F0")}")
            }
        }
    }

    private fun selectDefence(target: Person, impact: StandardImpact): Defence? {
        return target.template.defences
            .mapIndexed { index, reaction ->
                val cost = impact.defenceCost.get(reaction.resource)
                when {
                    cost == null -> null
                    cost > target.getResource(reaction.resource) -> null
                    else -> Defence(
                        resource = reaction.resource,
                        cost = cost,
                        onDrain = reaction.onDrain,
                        order = index
                    )
                }
            }
            .filterNotNull()
            .minBy { it.cost * 100 + it.order }
    }

    private data class Defence(
        val resource: Resource,
        val cost: Int,
        val onDrain: BonusOnDrain?,
        val order: Int
    )
}