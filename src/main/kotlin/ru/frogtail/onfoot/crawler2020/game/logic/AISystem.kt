package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonBehavior
import ru.frogtail.onfoot.crawler2020.game.state.resolveTurnOwner

class AISystem(
    private val game: Game,
    private val turnSystem: TurnSystem
) {
    fun beforeUpdate() {
        val actor = game.resolveTurnOwner()
        if (actor != null) {
            val stub = when (actor.behavior) {
                PersonBehavior.LocalPlayer -> Unit
                is PersonBehavior.AI.NoTargetAbilitySpammingDummy -> {
                    turnSystem.tryApplyAbility(actor.behavior.ability)
                }
            }
        }
    }
}