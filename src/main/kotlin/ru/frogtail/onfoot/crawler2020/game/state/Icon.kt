package ru.frogtail.onfoot.crawler2020.game.state

data class Icon(
    val path: String
)