package ru.frogtail.onfoot.crawler2020.game.state

import ru.frogtail.onfoot.crawler2020.game.state.display.Display
import ru.frogtail.onfoot.crawler2020.game.state.person.Person
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.ContextualIntent
import java.util.Deque
import kotlin.random.Random

class Game(
    val board: Board,
    val persons: MutableMap<PersonId, Person>,
    val turnQueue: Deque<PersonId>
) {
    val random = Random(123)
    val display = Display()
    var control = Control()
    var intent: ContextualIntent? = null
    var turn = 1L
}