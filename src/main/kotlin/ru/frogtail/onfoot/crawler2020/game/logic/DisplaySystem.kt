package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.state.Game

class DisplaySystem(
    private val game: Game
) {
    fun beforeUpdate() {
        game.display.log.removeIf { it.tillMillis < System.currentTimeMillis() }
        game.display.info.clear()
    }
}