package ru.frogtail.onfoot.crawler2020.game.data.games

import ru.frogtail.onfoot.crawler2020.game.data.abilities.Support
import ru.frogtail.onfoot.crawler2020.game.data.areas.Area1
import ru.frogtail.onfoot.crawler2020.game.data.persons.Cyclops
import ru.frogtail.onfoot.crawler2020.game.data.persons.Knight
import ru.frogtail.onfoot.crawler2020.game.data.persons.Norris
import ru.frogtail.onfoot.crawler2020.game.data.persons.Sorceress
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonBehavior
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.GameLauncher
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonAllocation
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonSlot

object GameArea1 {
    @JvmStatic
    fun main(args: Array<String>) {
        GameLauncher.startGame(Area1, mapOf(
            PersonSlot.A to PersonAllocation("Cyclops", Cyclops, PersonBehavior.LocalPlayer),
            PersonSlot.B to PersonAllocation("Sorceress", Sorceress, PersonBehavior.LocalPlayer),
            PersonSlot.C to PersonAllocation("Knight", Knight, PersonBehavior.LocalPlayer),
            PersonSlot.D to PersonAllocation("Dummy", Norris, PersonBehavior.AI.NoTargetAbilitySpammingDummy(Support.DivineIntervention))
        ))
    }
}