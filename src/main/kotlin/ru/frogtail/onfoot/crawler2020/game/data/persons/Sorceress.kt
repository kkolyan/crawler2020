package ru.frogtail.onfoot.crawler2020.game.data.persons

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Attack
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Movement
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Stance
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Support
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.DefenceReaction
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonTemplate
import ru.frogtail.onfoot.crawler2020.game.state.person.ResourceStack
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonTemplateHolder

object Sorceress : PersonTemplateHolder {
    override val template = PersonTemplate(
        name = "Sorceress",
        icon = Icon("Dungeon Crawl Stone Soup Full/monster/unique/erica_old.png"),
        maxHp = 20,
        abilities = listOf(
            Stance.TakeABreak,
            Stance.MissileStance,
            Movement.Walk,
            Movement.Jump,
            Attack.AttackNormal,
            Attack.AttackMissile,
            Support.Heal
        ),
        initialStacks = mapOf(
            ResourceSlot.PrimaryAttack to ResourceStack(Resource.MeleeAttack, 2),
            ResourceSlot.Mobility to ResourceStack(Resource.Dodge, 1),
            ResourceSlot.Concentration to ResourceStack(Resource.Tactic, 1),
            ResourceSlot.MagicSlot0 to ResourceStack(Resource.MagicRune01, 5)
        ),
        defences = listOf(
            DefenceReaction(Resource.Dodge),
            DefenceReaction(Resource.Parry)
        )
    )
}