package ru.frogtail.onfoot.crawler2020.misc.input.api

enum class Axis {
    MOUSE_X,
    MOUSE_Y,
    MOUSE_DELTA_X,
    MOUSE_DELTA_Y,
    MOUSE_WHEEL,
    MOUSE_DELTA_WHEEL,
    ;
}