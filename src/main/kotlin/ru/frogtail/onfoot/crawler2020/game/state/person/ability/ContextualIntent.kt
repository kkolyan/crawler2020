package ru.frogtail.onfoot.crawler2020.game.state.person.ability

sealed class ContextualIntent {
    data class Auto(val intent: AbilityIntent): ContextualIntent()
    data class Manual(val intent: AbilityIntent): ContextualIntent()
}