package ru.frogtail.onfoot.crawler2020.game.state.person.ability

enum class CounterParty {
    Enemy,
    Friend,
    Any,
}