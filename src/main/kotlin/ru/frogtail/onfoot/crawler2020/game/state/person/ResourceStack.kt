package ru.frogtail.onfoot.crawler2020.game.state.person

import ru.frogtail.onfoot.crawler2020.game.data.Resource

data class ResourceStack(
    val resource: Resource,
    val size: Int
)