package ru.frogtail.onfoot.crawler2020.game.state

import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.misc.collections.BiMap
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector

class Board(
    val width: Int,
    val height: Int,
    val background: Map<GridVector, Icon>,
    val walls: Map<GridVector, Icon>,
    val foreground: Map<GridVector, Icon>,
    val persons: BiMap<GridVector, PersonId>
) {
}