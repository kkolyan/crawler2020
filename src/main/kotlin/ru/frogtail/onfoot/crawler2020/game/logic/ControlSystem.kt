package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.data.abilities.Movement
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Stance
import ru.frogtail.onfoot.crawler2020.game.state.Control
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonBehavior
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.resolveTurnOwner
import ru.frogtail.onfoot.crawler2020.misc.input.api.Input
import ru.frogtail.onfoot.crawler2020.misc.input.api.KeyboardKey
import ru.frogtail.onfoot.crawler2020.misc.input.api.MouseButton
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector

class ControlSystem(
    private val input: Input,
    private val game: Game,
    private val turnSystem: TurnSystem
) : SceneControlHandler {

    fun beforeInput() {
        game.control = Control()
    }

    fun beforeUpdate() {
        if (input.getKey(KeyboardKey.KEY_F10).justPressed) game.control.quit = true

        val turnOwnerId = game.turnQueue.firstOrNull()
        game.intent = null
        when {
            turnOwnerId != null -> {
                val turnOwner = game.persons.getValue(turnOwnerId)
                val player = when(turnOwner.behavior) {
                    PersonBehavior.LocalPlayer -> true
                    is PersonBehavior.AI -> false
                }
                if (player) {
                    game.intent = turnSystem.calculateTarget(turnOwner)
                    when {
                        input.getKey(KeyboardKey.KEY_SPACE).justPressed -> tryTakeABreak()
                        input.getKey(KeyboardKey.KEY_W).justPressed -> tryMove(GridVector(0, 1))
                        input.getKey(KeyboardKey.KEY_S).justPressed -> tryMove(GridVector(0, -1))
                        input.getKey(KeyboardKey.KEY_A).justPressed -> tryMove(GridVector(-1, 0))
                        input.getKey(KeyboardKey.KEY_D).justPressed -> tryMove(GridVector(1, 0))
                        game.control.pointedAbility != null && input.getButton(MouseButton.LEFT).justPressed -> turnOwner.selectedAbility = game.control.pointedAbility
                        turnOwner.selectedAbility != null && input.getKey(KeyboardKey.KEY_ESCAPE).justPressed -> turnOwner.selectedAbility = null
                        turnOwner.selectedAbility != null && input.getButton(MouseButton.RIGHT).justPressed -> turnOwner.selectedAbility = null
                        turnOwner.selectedAbility != null && input.getButton(MouseButton.LEFT).justPressed -> turnSystem.applySelectedAbility()
                        turnOwner.selectedAbility == null && input.getButton(MouseButton.RIGHT).justPressed -> turnSystem.applyAutoAbility()
                        turnOwner.selectedAbility == null && input.getButton(MouseButton.LEFT).justPressed -> Unit
                    }
                }
            }
        }
    }

    private fun tryMove(delta: GridVector) {
        val actor = game.resolveTurnOwner()
        if (actor != null) {
            val currentPlace = game.board.persons.getA(actor.id)
            if (currentPlace != null) {
                val place = currentPlace.add(delta)
                turnSystem.tryApplyAbility(Movement.Walk, place)
            }
        }
    }

    private fun tryTakeABreak() {
        turnSystem.tryApplyAbility(Stance.TakeABreak)
    }

    override fun hoverAbilityButton(ability: Ability) {
        game.control.pointedAbility = ability
    }

    override fun hoverTile(location: GridVector) {
        game.control.pointedTile = location
    }

    override fun hoverPerson(personId: PersonId) {
        game.control.pointedPersonId = personId
    }
}