package ru.frogtail.onfoot.crawler2020.game.state

import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector

class Control {
    var pointedPersonId: PersonId? = null
    var pointedTile: GridVector? = null
    var pointedAbility: Ability? = null
    var quit: Boolean = false
}