package ru.frogtail.onfoot.crawler2020.game.data

enum class Range(
    val value: Float
) {
    Plus(1f),
    Square(1.5f),
    VeryFar(9001f),
    ;
}