package ru.frogtail.onfoot.crawler2020.game.state.person

import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability

data class PersonTemplate(
    // name of template, not of person
    val name: String,
    val icon: Icon,
    val maxHp: Int,
    val initialStacks: Map<ResourceSlot, ResourceStack>,
    val defences: List<DefenceReaction>,
    val abilities: List<Ability>
) {
    init {
        for (initialStack in initialStacks) {
            check(initialStack.key == initialStack.value.resource.slot) {
                "invalid initial stack config: $initialStack"
            }
        }
        for (ability in abilities) {
            val affectedResources = ability.cost.keys + ability.refund.keys
            for (affectedResource in affectedResources) {
                check (initialStacks.containsKey(affectedResource.slot)) {
                    "ability conflicts with available slots: $ability"
                }
            }
        }
        for (defence in defences) {
            check(initialStacks.contains(defence.resource.slot)) {
                "defence conflicts with available slots: $defence"
            }
        }
    }
}