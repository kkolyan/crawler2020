package ru.frogtail.onfoot.crawler2020.game.data

object LayoutConstants {
    const val FontAssetName = "Interface/Fonts/Default.fnt"
    const val LogicalScreenHeight = 12f
}