package ru.frogtail.onfoot.crawler2020.misc.input.ui

import com.jme3.scene.control.Control

interface HoverControl : Control {
    fun oHover()
}