package ru.frogtail.onfoot.crawler2020.game.data.persons

import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.data.ResourceSlot
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Attack
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Movement
import ru.frogtail.onfoot.crawler2020.game.data.abilities.Stance
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.DefenceReaction
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonTemplate
import ru.frogtail.onfoot.crawler2020.game.state.person.ResourceStack
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.BonusOnDrain
import ru.frogtail.onfoot.crawler2020.misc.gamrunner.PersonTemplateHolder

object Cyclops : PersonTemplateHolder {
    override val template = PersonTemplate(
        name = "Cyclops",
        icon = Icon("Dungeon Crawl Stone Soup Full/monster/unique/polyphemus_new.png"),
        maxHp = 20,
        abilities = listOf(
            Stance.TakeABreak,
            Stance.ReadyStance,
            Movement.Walk,
            Movement.Charge,
            Attack.AttackNormal,
            Attack.AttackHeavy
        ),
        initialStacks = mapOf(
            ResourceSlot.PrimaryAttack to ResourceStack(Resource.MeleeAttack, 2),
            ResourceSlot.Mobility to ResourceStack(Resource.Dodge, 2),
            ResourceSlot.Concentration to ResourceStack(Resource.Tactic, 1)
        ),
        defences = listOf(
            DefenceReaction(Resource.Dodge, BonusOnDrain(Resource.Speed, 1)),
            DefenceReaction(Resource.Parry)
        )
    )
}