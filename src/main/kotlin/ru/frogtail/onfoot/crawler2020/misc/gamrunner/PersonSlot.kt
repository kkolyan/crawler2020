package ru.frogtail.onfoot.crawler2020.misc.gamrunner

enum class PersonSlot {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    ;

    companion object {
        val ByChar: Map<Char, PersonSlot> = values().associateBy { it.name.toCharArray().single() }
    }
}