package ru.frogtail.onfoot.crawler2020.game.data.abilities

import ru.frogtail.onfoot.crawler2020.game.data.Range
import ru.frogtail.onfoot.crawler2020.game.data.Resource
import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityCommon
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.CounterParty
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.StandardImpact

object Attack {
    val AttackNormal = Ability.Person.Direct(
        counterParty = CounterParty.Enemy,
        range = Range.Square,
        impact = StandardImpact(
            damage = 5,
            defenceCost = mapOf(
                Resource.Block to 1,
                Resource.Parry to 1,
                Resource.Dodge to 1
            )
        ),
        common = AbilityCommon(
            name = "Attack",
            icon = Icon("Dungeon Crawl Stone Soup Full/gui/skills/short_blades.png"),
            cost = mapOf(Resource.MeleeAttack to 1),
            refund = mapOf(Resource.MeleeAttack to 1)
        )
    )

    val AttackHeavy = Ability.Person.Direct(
        counterParty = CounterParty.Enemy,
        range = Range.Square,
        impact = StandardImpact(
            damage = 10,
            defenceCost = mapOf(
                Resource.Block to 2,
                Resource.Parry to 1,
                Resource.Dodge to 1
            )
        ),
        common = AbilityCommon(
            name = "Heavy Attack",
            icon = Icon("Dungeon Crawl Stone Soup Full/gui/skills/maces_flails.png"),
            cost = mapOf(Resource.MeleeAttack to 2),
            refund = mapOf(Resource.MeleeAttack to 1)
        )
    )

    val AttackMissile = Ability.Person.Direct(
        counterParty = CounterParty.Enemy,
        range = Range.VeryFar,
        impact = StandardImpact(
            damage = 5,
            defenceCost = mapOf(
                Resource.Block to 1,
                Resource.Dodge to 2
            )
        ),
        common = AbilityCommon(
            name = "Missile Attack",
            icon = Icon("Dungeon Crawl Stone Soup Full/gui/skills/bows.png"),
            cost = mapOf(Resource.MissileAttack to 1),
            refund = mapOf(Resource.MissileAttack to 1)
        )
    )
}