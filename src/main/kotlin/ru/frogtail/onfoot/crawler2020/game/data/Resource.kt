package ru.frogtail.onfoot.crawler2020.game.data

import ru.frogtail.onfoot.crawler2020.game.state.Icon

enum class Resource(
    val slot: ResourceSlot,
    val icon: Icon
) {
    MeleeAttack(ResourceSlot.PrimaryAttack, Icon("Dungeon Crawl Stone Soup Full/item/weapon/blessed_blade.png")),
    Parry(ResourceSlot.PrimaryAttack, Icon("Dungeon Crawl Stone Soup Full/item/weapon/dagger_old.png")),
    MeleeAttackOnDemand(ResourceSlot.PrimaryAttack, Icon("Dungeon Crawl Stone Soup Full/item/weapon/orcish_great_sword.png")),
    MissileAttack(ResourceSlot.PrimaryAttack, Icon("Dungeon Crawl Stone Soup Full/item/weapon/hand_crossbow.png")),
    MissileAttackOnDemand(ResourceSlot.PrimaryAttack, Icon("Dungeon Crawl Stone Soup Full/item/weapon/hand_crossbow_2.png")),
    Block(ResourceSlot.Offhand, Icon("Dungeon Crawl Stone Soup Full/item/armor/shields/large_shield_3_new.png")),
    Dodge(ResourceSlot.Mobility, Icon("Dungeon Crawl Stone Soup Full/item/armor/shields/shield_2_kite.png")),
    Trick(ResourceSlot.Mobility, Icon("Dungeon Crawl Stone Soup Full/item/weapon/elven_dagger.png")),
    Speed(ResourceSlot.Mobility, Icon("Dungeon Crawl Stone Soup Full/item/weapon/bullwhip_old.png")),
    Tactic(ResourceSlot.Concentration, Icon("Dungeon Crawl Stone Soup Full/item/book/book_dog_eared.png")),
    MentalEffort(ResourceSlot.Concentration, Icon("Dungeon Crawl Stone Soup Full/gui/skills/charms.png")),
    MagicRune01(ResourceSlot.MagicSlot0, Icon("Dungeon Crawl Stone Soup Full/gui/skills/spellcasting.png")),
    ;

    companion object {
        val bySlot: Map<ResourceSlot, Set<Resource>> = values()
            .groupBy { it.slot }
            .mapValues { it.value.toSet() }
    }
}