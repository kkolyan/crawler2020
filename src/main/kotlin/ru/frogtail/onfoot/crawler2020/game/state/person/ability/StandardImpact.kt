package ru.frogtail.onfoot.crawler2020.game.state.person.ability

import ru.frogtail.onfoot.crawler2020.game.data.Resource

data class StandardImpact(
    val damage: Int? = null,
    val heal: Int? = null,
    val defenceCost: Map<Resource, Int> = mapOf()
)