package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.data.LayoutConstants
import ru.frogtail.onfoot.crawler2020.game.state.Game

class SceneLayout(
    private val game: Game,
    private val board: SceneBoard,
    private val abilitySelectBar: SceneAbilitySelectBar,
    private val display: SceneDisplay,
    private val abilityInfoPanel: SceneAbilityInfoPanel,
    private val personInfoPanel: ScenePersonInfoPanel
) {
    fun render(): Spatial {
        val node = Node("Root")

        board.render()
            .setLocalTranslation(0f, LayoutConstants.LogicalScreenHeight - game.board.height)
            .attachTo(node)

        abilitySelectBar.render()
            .setLocalTranslation(1f, 1.75f)
            .attachTo(node)

        personInfoPanel.render("Pointed Person", nameRowLabel = "Target:", resolvePersonId = { game.control.pointedPersonId })
            .setLocalTranslation(game.board.width.toFloat(), LayoutConstants.LogicalScreenHeight - ScenePersonInfoPanel.StandardHeight)
            .attachTo(node)

        personInfoPanel.render("Current Person", nameRowLabel = "Name:", resolvePersonId = { game.turnQueue.firstOrNull() })
            .setLocalTranslation(game.board.width.toFloat(), 0f)
            .attachTo(node)

        abilityInfoPanel.render()
            .setLocalTranslation(1f, 0.75f)
            .attachTo(node)

        display.render(width = 100f, height = LayoutConstants.LogicalScreenHeight - ScenePersonInfoPanel.StandardHeight * 2)
            .setLocalTranslation(game.board.width.toFloat(), ScenePersonInfoPanel.StandardHeight)
            .attachTo(node)

        return node
    }

}