package ru.frogtail.onfoot.crawler2020.game.state.person

import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability

sealed class PersonBehavior {
    object LocalPlayer : PersonBehavior()

    sealed class AI : PersonBehavior() {
        data class NoTargetAbilitySpammingDummy(
            val ability: Ability.NoTarget
        ) : AI()
    }
}