package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.logic.SceneControlHandler
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.misc.input.ui.addHoverControl
import ru.frogtail.onfoot.crawler2020.misc.scene.Icons

class SceneBoard(
    private val game: Game,
    private val assetManager: AssetManager,
    private val controlHandler: SceneControlHandler,
    private val sceneCursor: SceneCursor,
    private val scenePerson: ScenePerson
) {
    fun render(): Spatial {
        val node = Node("Field")
        val personsNode = Node("Persons")

        for ((cell, pic) in game.board.background) {
            Icons.createPicture(assetManager, pic)
                .setLocalTranslation(cell)
                .addHoverControl {
                    controlHandler.hoverTile(cell)
                }
                .attachTo(node)
        }

        // order matters
        node.attachChild(personsNode)

        for ((cell, pic) in game.board.walls) {
            Icons.createPicture(assetManager, pic)
                .setLocalTranslation(cell)
                .attachTo(node)
        }

        for ((cell, pic) in game.board.foreground) {
            Icons.createPicture(assetManager, pic)
                .setLocalTranslation(cell)
                .attachTo(node)
        }

        sceneCursor.render()
            .attachTo(node)

        val personSprites = mutableMapOf<PersonId, Spatial>()

        node.addUpdateControl {
            for (key in game.board.persons.b + personSprites.keys) {
                val place = game.board.persons.getA(key)
                var sprite = personSprites.get(key)
                if (place == null) {
                    personsNode.detachChild(sprite)
                }
                if (sprite == null) {
                    sprite = scenePerson.render(key)
                    personSprites.put(key, sprite)
                    personsNode.attachChild(sprite)
                }
            }
        }
        return node
    }
}