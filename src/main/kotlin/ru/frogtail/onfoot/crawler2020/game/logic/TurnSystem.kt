package ru.frogtail.onfoot.crawler2020.game.logic

import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.person.Person
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.Ability
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.AbilityIntent
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.ContextualIntent
import ru.frogtail.onfoot.crawler2020.game.state.person.ability.isApplicable
import ru.frogtail.onfoot.crawler2020.game.state.resolveTurnOwner
import ru.frogtail.onfoot.crawler2020.misc.math.GridVector

class TurnSystem(
    private val game: Game,
    private val impactSystem: ImpactSystem,
    private val abilitySystem: AbilitySystem
) {

    fun beforeUpdate() {
        applySelectedNoTargetAbility()
    }

    fun calculateTarget(actor: Person): ContextualIntent? {
        val selectedAbility = actor.selectedAbility
        val targetTile = game.control.pointedTile
        val targetPersonId = game.control.pointedPersonId

        if (selectedAbility != null) {
            return abilitySystem.aim(actor, selectedAbility, targetTile, targetPersonId)
                .let { ContextualIntent.Manual(it) }
        }

        for (ability in actor.template.abilities) {
            val aiming = when (ability) {
                is Ability.NoTarget -> null
                is Ability.Place,
                is Ability.Person ->
                    abilitySystem.aim(actor, ability, targetTile, targetPersonId)
            }
            if (aiming != null && aiming.isApplicable()) {
                return ContextualIntent.Auto(aiming)
            }
        }

        return null
    }

    fun applyAutoAbility() {
        val action = game.intent
        if (action != null) {
            check(action is ContextualIntent.Auto)
            val success = when (action.intent) {
                is AbilityIntent.Invalid -> null
                is AbilityIntent.Valid -> action.intent
            }
            if (success != null) {
                applyAbility(success)
            }
        }
    }

    fun applySelectedAbility() {
        val action = game.intent
        if (action != null) {
            check(action is ContextualIntent.Manual)
            val success = when (action.intent) {
                is AbilityIntent.Invalid -> null
                is AbilityIntent.Valid -> action.intent
            }
            if (success != null) {
                applyAbility(success)
            }
        }
    }

    fun tryApplyAbility(ability: Ability.NoTarget) {
        tryApplyAbilityInternal(ability)
    }

    fun tryApplyAbility(ability: Ability.Place, targetTile: GridVector) {
        tryApplyAbilityInternal(ability, targetTile = targetTile)
    }

    private fun applySelectedNoTargetAbility() {
        val preparedAction = game.intent
        if (preparedAction != null) {
            val noTargetAiming = when (preparedAction) {
                is ContextualIntent.Manual -> {
                    when (preparedAction.intent) {
                        is AbilityIntent.Valid.NoTarget -> {
                            preparedAction.intent
                        }
                        is AbilityIntent.Valid.Place -> null
                        is AbilityIntent.Valid.Person -> null
                        is AbilityIntent.Invalid -> null
                    }
                }
                is ContextualIntent.Auto -> null // cannot auto-apply auto-selected action
            }
            if (noTargetAiming != null) {
                applyAbility(noTargetAiming)
            }
        }
    }

    private fun tryApplyAbilityInternal(ability: Ability, targetTile: GridVector? = null, targetPersonId: PersonId? = null) {
        val actor = game.resolveTurnOwner()
        if (actor != null) {
            val aiming = abilitySystem.aim(actor, ability, targetTile, targetPersonId)
            val success = when (aiming) {
                is AbilityIntent.Invalid -> null
                is AbilityIntent.Valid -> aiming
            }
            if (success != null) {
                applyAbility(success)
            }
        }
    }

    private fun applyAbility(action: AbilityIntent.Valid) {
        val actor = game.resolveTurnOwner()
            ?: error("WTF")
        abilitySystem.trigger(actor, action)
        actor.selectedAbility = null
        endTurn()
    }

    private fun endTurn() {
        val turnOwner = game.turnQueue.firstOrNull() ?: error("WTF")
        impactSystem.beforeEndTurn(game.persons.getValue(turnOwner))
        if (game.turnQueue.remove(turnOwner)) {
            game.turnQueue.addLast(turnOwner)
        }
        val newTurnOwner = game.turnQueue.firstOrNull()
        if (newTurnOwner != null) {
            impactSystem.beforeStartTurn(game.persons.getValue(newTurnOwner))
        }
        game.turn++
    }
}