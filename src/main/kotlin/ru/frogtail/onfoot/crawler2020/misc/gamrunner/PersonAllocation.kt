package ru.frogtail.onfoot.crawler2020.misc.gamrunner

import ru.frogtail.onfoot.crawler2020.game.state.person.PersonBehavior
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonTemplate

data class PersonAllocation(
    val name: String,
    val person: PersonTemplate,
    val behavior: PersonBehavior
) {
    constructor(
        name: String,
        person: PersonTemplateHolder,
        behavior: PersonBehavior
    ) : this(name, person.template, behavior)
}