package ru.frogtail.onfoot.crawler2020.game.visual

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.scene.Spatial
import ru.frogtail.onfoot.crawler2020.game.state.Game
import ru.frogtail.onfoot.crawler2020.game.state.color
import ru.frogtail.onfoot.crawler2020.game.state.person.PersonId

class ScenePersonInfoPanel(
    private val assetManager: AssetManager,
    private val game: Game
) {

    fun render(name: String, nameRowLabel: String, resolvePersonId: () -> PersonId?): Spatial {
        return createReactiveNode(
            name = name,
            resolveState = { game.persons.get(resolvePersonId()) },
            renderContent = { node, person ->
                if (person != null) {
                    val tableData = mapOf(
                        nameRowLabel to person.name.color("AAA"),
                        "HP:" to "${person.hp} / ${person.template.maxHp}".color("AAA")
                    )
                    Tables.renderTable(assetManager, tableData)
                        .attachTo(node)
                        .setLocalTranslation(0f, 2f)

                    for ((i, stack) in person.stacks.values.withIndex()) {
                        assetManager.loadIcon(stack.resource.icon)
                            .setLocalTranslation(i * 1f, 1f)
                            .attachTo(node)
                        BitmapText(assetManager.loadDefaultFont())
                            .also {
                                it.text = stack.size.toString()
                                it.setBox(Rectangle(i * 1f, 1f, 1f, 1f))
                                it.size = 0.5f
                                it.alignment = BitmapFont.Align.Center
                                it.verticalAlignment = BitmapFont.VAlign.Center
                            }
                            .attachTo(node)
                    }
                }
            }
        )
    }

    companion object {
        const val StandardHeight = 3f
    }

}