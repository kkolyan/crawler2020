package ru.frogtail.onfoot.crawler2020.misc.collections

class BiMap<A,B> {
    private val a2b = mutableMapOf<A, B>()
    private val b2a = mutableMapOf<B, A>()

    val a: Set<A> get() = a2b.keys
    val b: Set<B> get() = b2a.keys

    fun newLink(a: A, b:B) {
        val prevB = a2b.get(a)
        check(prevB == null) {
            "$a cannot be linked to $b, because already linked to $prevB"
        }
        val prevA = b2a.get(b)
        check(prevA == null) {
            "$b cannot be linked to $a, because already linked to $prevA"
        }

        a2b.put(a, b)
        b2a.put(b, a)

    }

    fun unlinkA(a: A) {
        val prevB = a2b.remove(a)
        b2a.remove(prevB)
    }

    fun unlinkB(b: B) {
        val prevA = b2a.remove(b)
        a2b.remove(prevA)
    }

    fun reLink(a: A, b:B) {
        val prevB = a2b.get(a)
        val prevA = b2a.get(b)
        b2a.remove(prevB)
        a2b.remove(prevA)
        a2b.put(a, b)
        b2a.put(b, a)
    }

    fun getB(a: A): B? {
        return a2b.get(a)
    }

    fun getA(a: B): A? {
        return b2a.get(a)
    }
}