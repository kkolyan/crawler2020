package ru.frogtail.onfoot.crawler2020.game.data.areas

import ru.frogtail.onfoot.crawler2020.game.state.Icon
import ru.frogtail.onfoot.crawler2020.misc.gameloaders.LoadableArea
import ru.frogtail.onfoot.crawler2020.misc.gameloaders.TextBoardGameLoader

object Area1 : LoadableArea {
    override val loader = TextBoardGameLoader(
        background = Icon("Dungeon Crawl Stone Soup Full/dungeon/floor/rect_gray_2_old.png"),
        walls = mapOf(
            'x' to Icon("Dungeon Crawl Stone Soup Full/dungeon/wall/brick_brown_2.png")
        ),
        hStep = 3,
        map = """
            x  x  x  x  x  x  x  x  x  x  x  x  x  x  x  x 
            x              D                             x 
            x     x  x  B     C  x  x  x  x  x  x  x     x 
            x              A                       x     x
            x     x     x     x     x     x  x  x  x     x 
            x     x     x     x     x              x     x 
            x     x                 x                    x 
            x     x     x     x     x              x  x  x
            x  x  x  x  x  x  x  x  x  x  x  x  x  x  x  x
                """.trimIndent()
    )
}